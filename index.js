const express = require('express')

const app = express();

const port = process.env.port || 3000

 app.use(express.json())

  app.use(express.urlencoded({extended: true}))

   app.listen(port, () => console.log(`Server Running at Localhost: ${port}`))




//------- Activity No. 1 - creat GET route that will access /home---

app.get("/home", (request,response) => {
	response.send('Welcome to the homepage!')
})


//------- Activity No. 3 - creat GET route that will access /users that will retrieve users in mock database---

let users = []

app.get("/users", (request, response) => {
	if (request.body.username !== '' && request.body.password !== '')
	{
		users.push(request.body)
		response.send(users)
		
	}
	else
	{
		response.send('Please input BOTH username and password!')
	}
})


// /------- Activity No. 3 - create a delete route that will access /delete-user to remove a user from the mock database---

// let users = [
// 	{
// 		"username": "Jeff Dahmer",
// 		"password": "Jeff"
// 	},
// 	{
// 		"username": "Charles Manson",
// 		"password": "Charles"
//     }
// ]


app.delete("/delete-user", (request, response) => {
	let message = 'User not exist'
	for(let i =0; i < users.length; i++){
			if(request.body.username == users[i].username){
            users.splice(i, 1);}    
			message = `User ${request.body.username}  has been deleted!`
			break;
   }
   response.send(message)
})
